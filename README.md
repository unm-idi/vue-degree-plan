# vue-degree-plan

> This plugin is leveraged by the blackbriar ecosystem to visualize degree plans in tabular, graph, and grid formats.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build demo (used to prepare for gitlab pages)
npm run build:demo

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
