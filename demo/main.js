// This file will build the demo
import Vue from 'vue'
import VModal from 'vue-js-modal'
import Demo from './Demo'

// required vue-js-modal plugin must be installed with the default component name of 'modal'
Vue.use(VModal)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<Demo/>',
  components: { Demo }
})
