export default {
  props: {
    headerInnerWidth: {
      type: Number,
      required: true
    },

    headerInnerHeight: {
      type: Number,
      required: true
    },

    itemCellX: {
      type: Function,
      required: true
    },

    itemCellY: {
      type: Function,
      required: true
    },

    itemCenterX: {
      type: Function,
      required: true
    },

    itemCenterY: {
      type: Function,
      required: true
    },

    margin: {
      type: Number,
      required: true
    },

    radius: {
      type: Number,
      required: true
    }
  }
}
