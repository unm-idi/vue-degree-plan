import { get, includes, some, uniq } from 'lodash'
import planItemNames from '../helpers/planItemNames.js'

export default {
  props: {
    item: {
      type: Object,
      required: true
    },

    selected: {
      type: Boolean,
      default: false
    },

    hoveredPlanItem: {
      type: Object
    }
  },

  computed: {
    req () {
      return this.item.requirement
    },

    showMinimumGrade () {
      return this.item.requirement && ['CourseRequirement', 'WildCardRequirement'].includes(this.item.requirement.type)
    },

    names () {
      return planItemNames(this.item)
    },

    myCourse () {
      return get(this, ['item', 'plan_item_spec', 'course']) ||
             get(this, ['item', 'requirement', 'course'])
    },

    planItemSpecCourses () {
      return this.traversePlanItemSpec()
    },

    courseRequirements () {
      return this.traverseCourseRequirements()
    },

    planItemSpecMinimumGrade () {
      let minimumGrades = this.planItemSpecCourses.map(course => {
        const requirement = this.courseRequirements.find(courseRequirement => courseRequirement.course.id === course.id)
        return requirement ? requirement.minimum_grade : null
      })
      const mismatchFound = minimumGrades.includes(null)
      minimumGrades = uniq(minimumGrades.filter(minimumGrade => minimumGrade))
      return minimumGrades.length === 1 && !mismatchFound ? minimumGrades[0] : null
    },

    requirementMinimumGrade () {
      var minimumGrades = []
      for (var i = 0; i < this.courseRequirements.length; i++) {
        minimumGrades.push(this.courseRequirements[i].minimum_grade)
      }
      // this.sortMinimumGrades(minimumGrades)
      return minimumGrades[0]
    },

    // if there is a hovered plan item, find its association to `this` item
    hoveredAssociation () {
      const sources = this.item.sources
      const targets = this.item.targets
      const hovered = this.hoveredPlanItem
      if (hovered) {
        if (this.item === this.hoveredPlanItem) {
          return 'this-item'
        } else if (includes(sources.all, hovered)) {
          return 'unblocked'
        } else if (this.scanField('sources')) {
          return 'unblocked-field'
        } else if (includes(targets.prereq, hovered)) {
          return 'prereq'
        } else if (includes(targets.coreq, hovered)) {
          return 'coreq'
        } else if (includes(targets.strictCoreq, hovered)) {
          return 'strict-coreq'
        } else if (this.scanField('targets')) {
          return 'pre-coreq-field'
        } else {
          return 'no-association'
        }
      }
    }
  },

  methods: {
    // type: 'sources' or 'targets'
    scanField (type, item = this.item, alreadyScanned = []) {
      return some(item[type].all, i => {
        if (includes(alreadyScanned, i)) return
        alreadyScanned.push(i)
        return i === this.hoveredPlanItem || this.scanField(type, i, alreadyScanned)
      })
    },
    // flatten out plan item spec courses
    traversePlanItemSpec (planItemSpec = this.item.plan_item_spec, courses = []) {
      if (!planItemSpec) {
        return courses
      } else if (planItemSpec.operands) {
        planItemSpec.operands.forEach(operand => this.traversePlanItemSpec(operand, courses))
      } else if (planItemSpec.course) {
        courses.push(planItemSpec.course)
      }
      return courses
    },
    // flatten out course requirements
    traverseCourseRequirements (requirement = this.req, courseRequirements = []) {
      if (!requirement) {
        return courseRequirements
      }
      switch (requirement.type) {
        case 'CourseRequirement':
          courseRequirements.push(requirement)
          break
        case 'DegreeRequirement':
          requirement.subrequirements.forEach(subRequirement => this.traverseCourseRequirements(subRequirement, courseRequirements))
          break
        case 'CoreRequirement':
          this.traverseCourseRequirements(requirement.core, courseRequirements)
          break
      }
      return courseRequirements
    },

    sortMinimumGrades (grades) {
      grades.sort((a, b) => {
        const order = { '+': -1, '-': 1, undefined: 0 }
        // console.log(a[0].localeCompare(b[0]) || order[a[1]] - order[b[1]])
        return a[0].localeCompare(b[0]) || order[a[1]] - order[b[1]]
      })
    }
  }
}
