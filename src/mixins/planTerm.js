import { sortBy } from 'lodash'

export default {
  data () {
    return {
      touchStartTime: null
    }
  },

  props: {
    term: {
      type: Object,
      required: true
    },

    selectedPlanItem: {
      type: Object
    },

    hoveredPlanItem: {
      type: Object
    },
    termIdentifier: {
      type: String
    },
    customTermName: {
      type: Boolean
    },
    termNumber: {
      type: Number
    }
  },

  computed: {
    sortedPlanItems () {
      return sortBy(this.term.plan_items, 'position')
    },
    termName () {
      return this.customTermName ? this.term.name : `${this.termIdentifier} ${this.termNumber}`
    }
  },

  methods: {
    touchStart (item) {
      this.touchStartTime = Date.now()
      this.$emit('update:hoveredPlanItem', item)
    },

    touchEnd (item) {
      this.$emit('update:hoveredPlanItem', null)
      if (Date.now() - this.touchStartTime < 300) {
        this.$emit('update:selectedPlanItem', item)
      }
    },

    mouseOver (item) {
      if (this.touchStartTime) return
      this.$emit('update:hoveredPlanItem', item)
    },

    mouseOut () {
      if (this.touchStartTime) return
      this.$emit('update:hoveredPlanItem', null)
    },

    click (item) {
      if (this.touchStartTime) return
      this.$emit('update:selectedPlanItem', item)
    }
  }
}
