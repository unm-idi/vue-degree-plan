import { get } from 'lodash'
import { requisiteStringify } from '@unm-idi/vue-course-requisite'

// fetch the plan item names and return them in the form:
// {
//   longName // e.g. 'ECE 101 - Intro to Engineering' or 'A Requirement Name' or 'ECE 101 or ECE 102'
//   code // e.g. 'ECE 101' or null
//   name // e.g. 'Intro to Engineering' or 'A Requirement' or 'ECE 101 or ECE 102'
// }
export default function (planItem) {
  const names = {}

  names.longName = get(planItem, 'plan_item_spec.course.full_name') ||
                   requisiteStringify(planItem.plan_item_spec) ||
                   get(planItem, 'requirement.course.full_name') ||
                   get(planItem, 'requirement.name') ||
                   planItem.name

  names.code = get(planItem, 'plan_item_spec.course.full_number') ||
               get(planItem, 'requirement.course.full_number')

  names.name = get(planItem, 'plan_item_spec.course.name') ||
               requisiteStringify(planItem.plan_item_spec) ||
               get(planItem, 'requirement.plan_item_spec.course.name') ||
               get(planItem, 'requirement.course.name') ||
               planItem.name ||
               get(planItem, 'requirement.name')

  return names
}
