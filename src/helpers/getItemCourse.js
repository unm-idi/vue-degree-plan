import { get } from 'lodash'

export default function (item) {
  return get(item, ['plan_item_spec', 'course']) ||
         get(item, ['requirement', 'course'])
}
