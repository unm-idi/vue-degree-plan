import { some } from 'lodash'

// scan for a course in a prereq / coreq root operand

export const scan = function (course, operand, allowConcurrency = false) {
  if (operand && operand.type === 'course') {
    return course.id === operand.course.id && (!allowConcurrency || operand.concurrency_ind)
  } else if (operand && ['or', 'and'].indexOf(operand.type) > -1) {
    return some(operand.operands, o => scan(course, o, allowConcurrency))
  } else {
    return false
  }
}
