import { flatten, sortBy } from 'lodash'
import getItemCourse from './getItemCourse'
import { scan } from './requisite'

// Function used to prepare term data for use by vue-degree-plan components.
// * Adds sources and targets to terms[].plan_items[]
// * Adds term reference to terms[].plan_items[]
export default function (terms) {
  const items = flatten(sortBy(terms, 'position').map((t, ti) => {
    t.position = ti
    return sortBy(t.plan_items, 'position').map((i, ii) => {
      i.position = ii
      i.sources = {
        all: [],
        prereq: [],
        coreq: [],
        strictCoreq: []
      }

      i.targets = {
        all: [],
        prereq: [],
        coreq: [],
        strictCoreq: []
      }

      i.term = t

      return {
        item: i,
        course: getItemCourse(i)
      }
    })
  }))

  items.forEach(s => {
    items.forEach(t => {
      if (!s.course || !t.course) return
      else if (s.course.coreq && scan(t.course, s.course.coreq)) {
        s.item.sources.all.push(t.item)
        s.item.sources.strictCoreq.push(t.item)
        t.item.targets.all.push(s.item)
        t.item.targets.strictCoreq.push(s.item)
      } else if (s.course.prereq && scan(t.course, s.course.prereq, true)) {
        s.item.sources.all.push(t.item)
        s.item.sources.coreq.push(t.item)
        t.item.targets.all.push(s.item)
        t.item.targets.coreq.push(s.item)
      } else if (s.course.prereq && scan(t.course, s.course.prereq)) {
        s.item.sources.all.push(t.item)
        s.item.sources.prereq.push(t.item)
        t.item.targets.all.push(s.item)
        t.item.targets.prereq.push(s.item)
      }
    })
  })

  return terms
}
